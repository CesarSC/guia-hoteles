$("[data-toggle='tooltip']").tooltip();
$("[data-toggle='popover']").popover();
$('.carousel').carousel({
  interval: 2000
})

$('#contacto').on('show.bs.modal', (event)=>{
  console.log('el modal se esta mostrando');

  $('#contactoBtn').removeClass('btn-outline-success');
  $('#contactoBtn').addClass('btn-primary');
  $('#contactoBtn').prop('disabled', true);
})

$('#contacto').on('shown.bs.modal', (event)=>{
  console.log('el modal se mostro');
})

$('#contacto').on('hide.bs.modal', (event)=>{
  console.log('el modal se esta ocultando');
})

$('#contacto').on('hidden.bs.modal', (event)=>{
  console.log('el modal se oculto');
  $('#contactoBtn').addClass('btn-outline-success');
  $('#contactoBtn').removeClass('btn-primary');
  $('#contactoBtn').prop('disabled', false);
})